import React, {Component} from 'react';
import ReCAPTCHA from "react-google-recaptcha";

class Form extends Component {
    state = {
        fields: {
            FirstName: {
                value: "",
                error: null
            },
            LastName: {
                value: "",
                error: null
            },
            Email: {
                value: '',
                error: null
            },
            Agreement: {
                value: false,
                error: null
            },
            Country: {
                value: 'choose',
                error: null
            }
        },
        formSend: false
    }
    handleSubmit = (e) => {
        e.preventDefault();

        let errors = 0;
        let data = {};
        const url = "https://formularz-test.firebaseio.com/data.json";

        for (let field in this.state.fields) {
            errors += this.validateField(field, this.state.fields[field].value);

            var newElement = {[field]: this.state.fields[field].value};
            data = {...data, ...newElement};
        }
        let gcresponse = window.grecaptcha.getResponse();
        if (gcresponse.length === 0) {
            errors++;
        }
        // console.log(errors);

        if (errors > 0) {
            return;
        } else {
            fetch(url, {
                method: 'POST',
                body: JSON.stringify(data),
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(res => {
                if (res.status === 200) {
                    this.setState({
                        formSend: true
                    });
                } else {
                    console.log("Status: " + res.status)
                }
            })
            .catch(error => console.log("Błąd: ", error));
        }
    }
    handleChange = (e) => {
        var fieldValue;
        var fieldName = e.target.name;
        if (fieldName === 'Agreement') {
            fieldValue = e.target.checked;
        } else {
            fieldValue = e.target.value.trim();
        }
        this.validateField(fieldName, fieldValue);
    }
    validateField = (fieldName, fieldValue) => {
        const emailRegexp = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

        const changedFields = {
            ...this.state.fields
        }
        changedFields[fieldName].value = fieldValue;

        if (fieldName === "Email" && fieldValue !== "") {
            if (!emailRegexp.test(fieldValue)) {
                changedFields[fieldName].error = "Email is incorrect";
            } else {
                changedFields[fieldName].error = null;
            }
        } else if ((fieldName === "Country" && fieldValue.toLowerCase() === "choose") || (fieldName === "Agreement" && fieldValue !== true) || (fieldValue === "" || fieldValue === null)) {
            changedFields[fieldName].error = fieldName + " is required.";
        } else {
            changedFields[fieldName].error = null;
        }
        this.setState({
            fields: changedFields
        });
        return changedFields[fieldName].error !== null ? 1 : 0;
    }
    render() {
        return (
            <div className="form">
                {this.state.formSend === true ? (
                    <div className="alert alert-success">Thank you. Bla bla bla.</div>
                ) : (
                    <form onSubmit={this.handleSubmit} name="myForm">
                        <div className="form-group">
                            <label htmlFor="FirstName">First name:</label>
                            <input onChange={this.handleChange} className={(this.state.fields.FirstName.error ? "is-invalid " : "") + "form-control"} type="text" name="FirstName" id="FirstName" placeholder="Enter first name" />
                            {this.state.fields.FirstName.error ? (
                                <span className="error">{this.state.fields.FirstName.error}</span>
                            ) : null}
                        </div>
                        <div className="form-group">
                            <label htmlFor="LastName">Last name:</label>
                            <input onChange={this.handleChange} className={(this.state.fields.LastName.error ? "is-invalid " : "") + "form-control"} type="text" name="LastName" id="LastName" placeholder="Enter last name" />
                            {this.state.fields.LastName.error ? (
                                <span className="error">{this.state.fields.LastName.error}</span>
                            ) : null}
                        </div>
                        <div className="form-group">
                            <label htmlFor="Email">Email:</label>
                            <input onChange={this.handleChange} className={(this.state.fields.Email.error ? "is-invalid " : "") + "form-control"} type="email" name="Email" id="Email" placeholder="Enter your email" />
                            {this.state.fields.Email.error ? (
                                <span className="error">{this.state.fields.Email.error}</span>
                            ) : null}
                        </div>
                        <div className="form-group">
                            <label htmlFor="Country">Country:</label>
                            <select onChange={this.handleChange} className={(this.state.fields.Country.error ? "is-invalid " : "") + "form-control"} name="Country" id="Country">
                                <option>Choose</option>
                                <option>Poland</option>
                                <option>Scotland</option>
                                <option>Noland</option>
                            </select>
                            {this.state.fields.Country.error ? (
                                <span className="error">{this.state.fields.Country.error}</span>
                            ) : null}
                        </div>
                        <input type="checkbox" name="Agreement" id="Agreement" onChange={this.handleChange} />
                        <label htmlFor="Agreement">Ja sie zgadzam</label>
                        <br/>
                        {this.state.fields.Agreement.error ? (
                            <span className="error">{this.state.fields.Agreement.error}</span>
                        ) : null}
                        <ReCAPTCHA
                            sitekey="6LdJFLYUAAAAANQMBiUFJc-gmxHaaF4TzsYJvlIe"
                            onChange={this.captchaOnChange}
                        />
                        <br/>
                        <input type="submit" name="Send" className="btn btn-primary" value="SEND" />
                    </form>
                )}
            </div>
        )
    }
}
export default Form;
