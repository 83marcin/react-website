import React, { Component } from 'react';
import { Link } from "react-router-dom";

class Post extends Component {
    state = {
        data: null
    }
    componentDidMount() {
        fetch("https://jsonplaceholder.typicode.com/posts/" + this.props.match.params.id)
            .then(response => response.json())
            .then(newResponse => {
                    this.setState({
                        data: newResponse
                    })
                }
            );
        console.log(this.props);
    }
    render() {
        return (
            <div className="post">
                {this.state.data ? (
                    <div>
                        <h1>{this.state.data.title}</h1>
                        <div>{this.state.data.body}</div>
                    </div>
                ) : null}
                <Link to={"/posts"}>back</Link>
            </div>
        );
    }
}
export default Post;
