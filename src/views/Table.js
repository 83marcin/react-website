import React, { Component } from 'react';

class Table extends Component {
    state = {
        data: null
    }

    componentDidMount() {
        fetch("https://formularz-test.firebaseio.com/data.json")
        .then(response => response.json())
        .then(response => {
            this.setState({
                data: response
            })
        })
        .catch(error => console.log("Błąd: ", error));
    }
    render(){
        return(
            <div>
                {this.state.data ? (
                    <table className="table table-striped">
                        <tbody>
                            {Object.keys(this.state.data).map((item, index) => {
                                return (
                                    <tr key={index}>
                                        <td>{this.state.data[item].FirstName}</td>
                                        <td>{this.state.data[item].LastName}</td>
                                        <td>{this.state.data[item].Email}</td>
                                        <td>{this.state.data[item].Country}</td>
                                    </tr>
                                );
                            })}
                        </tbody>
                    </table>
                ) : null}
            </div>
        );
    }
}
export default Table;