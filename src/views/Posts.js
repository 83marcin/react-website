import React, { Component } from 'react';
import { Link } from "react-router-dom";

class Posts extends Component {
    state = {
        data: null
    }
    componentDidMount() {
        fetch("https://jsonplaceholder.typicode.com/posts")
            .then(response => response.json())
            .then(newResponse => {
                    this.setState({
                        data: newResponse
                    })
                }
            );
    }
    render () {
        return (
            <div className="posts">
                {this.state.data ? (
                    <ul>
                        {this.state.data.map(post => {
                            return <li key={post.id}><Link to={"/posts/" + post.id}>{post.title}</Link></li>
                        })}
                    </ul>
                ) : null}
            </div>
        );
    }
}
export default Posts;
