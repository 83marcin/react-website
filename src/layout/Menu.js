import React from 'react';
import { NavLink } from "react-router-dom";

const menu = () => {
    return (
        <ul className="nav flex-column nav-pills">
            <li className="nav-item">
                <NavLink className="nav-link" exact to="/">Home</NavLink>
            </li>
            <li className="nav-item">
                <NavLink className="nav-link" to="/posts">Posts</NavLink>
            </li>
            <li className="nav-item">
                <NavLink className="nav-link" to="/photos">Photos</NavLink>
            </li>
            <li className="nav-item">
                <NavLink className="nav-link" to="/form">Form</NavLink>
            </li>
            <li className="nav-item">
                <NavLink className="nav-link" to="/table">Table</NavLink>
            </li>
        </ul>
    );
}
export default menu;
