import React from 'react';
import Home from '../views/Home';
import Posts from '../views/Posts';
import Post from '../views/Post';
import Photos from '../views/Photos';
import Form from '../views/Form';
import Table from '../views/Table';
import { Route, Switch } from "react-router-dom";

const content = () => {
    return (
        <div className="page-content">
            <Switch>
                <Route exact path="/" component={Home} />
                <Route exact path="/photos" component={Photos} />
                <Route exact path="/posts" component={Posts} />
                <Route exact path="/posts/:id" component={Post} />
                <Route exact path="/form" component={Form} />
                <Route exact path="/table" component={Table} />
            </Switch>
        </div>
    );
}
export default content;
