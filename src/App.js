import React from 'react';
import 'bootstrap/dist/css/bootstrap.css';
import Menu from './layout/Menu';
import Content from './layout/Content';
import { BrowserRouter } from "react-router-dom";

function App() {
  return (
    <BrowserRouter>
      <div className="App">
        <div className="container">
          <div className="row">
            <div className="col-md-3">
              <Menu />
            </div>
            <div className="col-md-9">
              <Content />
            </div>
          </div>
        </div>
      </div>

    </BrowserRouter>
  );
}

export default App;
